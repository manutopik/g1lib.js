import test from 'ava';
import * as app from './dictionary-tree.mjs';

const buildTreeThenSerialize = str => app.serialize(app.buildTreeStruct(str));

test('simple string still simple string', t => t.is(buildTreeThenSerialize('abc'), 'abc'));
test('(a|b) alt still (a|b)', t => t.is(buildTreeThenSerialize('(a|b)'), '(a|b)'));
test('a)b throw', t => t.throws(() => app.buildTreeStruct('a)b')));
// Ok to be permissive test('(a throw',t=>t.throws(()=>buildTreeStruct('(a')));
// Ok to be permissive test('a|b throw',t=>t.throws(()=>buildTreeStruct('a|b')));
test('(|b) keep empty choice', t => t.is(buildTreeThenSerialize('(|b)'), '(|b)'));
test('(b|b) trivial dedup', t => t.is(buildTreeThenSerialize('(|b||b|)'), '(|b)'));
test('a(b|c) mix fix and alt', t => t.is(buildTreeThenSerialize('a(b|c)'), 'a(b|c)'));
test('a(b) flat merge when no alt', t => t.is(buildTreeThenSerialize('a(b)'), 'ab'));
test('build complexe tree with (|) pattern', t => t.is(buildTreeThenSerialize('(a(|b@@@@c|d|)|(e|f)|g|h@@@@i)'), '(a(|b@@@@c|d)|e|f|g|h@@@@i)'));

test('serialize incorrect tree throw', t => t.throws(() => app.serialize({plop: ['a']})));

test('mono altCount', t => t.is(app.altCount(app.buildTreeStruct('ipsum')), 1));
test('simple altCount', t => t.is(app.altCount(app.buildTreeStruct('(lore|ipsu)m')), 2));
test('multi altCount', t => t.is(app.altCount(app.buildTreeStruct('(a|b|c)(d|e|f)g(h|i|j|k)')), 36));
test('multi level tree altCount', t => t.is(app.altCount(app.buildTreeStruct('a(b(c|d)|e(f|g|h)ij(k|l)|@@@@m)')), 9));

const exampleTree = () => app.buildTreeStruct('a(b(c|d)|e(f|g(h|i)|j)kl(m|n(o|p)|q(r|s)|t)|(u|v)w)');
// console.log(JSON.stringify(exampleTree()));
// console.log(app.serialize(exampleTree()));
test('getAlternative 0', t => t.is(app.getAlternative(0, exampleTree()), 'abc'));
test('getAlternative 1', t => t.is(app.getAlternative(1, exampleTree()), 'abd'));
test('getAlternative 2', t => t.is(app.getAlternative(2, exampleTree()), 'aefklm'));
test('getAlternative 3', t => t.is(app.getAlternative(3, exampleTree()), 'aefklno'));
test('getAlternative 4', t => t.is(app.getAlternative(4, exampleTree()), 'aefklnp'));
test('getAlternative 5', t => t.is(app.getAlternative(5, exampleTree()), 'aefklqr'));
test('getAlternative 6', t => t.is(app.getAlternative(6, exampleTree()), 'aefklqs'));
test('getAlternative 7', t => t.is(app.getAlternative(7, exampleTree()), 'aefklt'));
test('getAlternative 8', t => t.is(app.getAlternative(8, exampleTree()), 'aeghklm'));
test('getAlternative 9', t => t.is(app.getAlternative(9, exampleTree()), 'aeghklno'));
test('getAlternative 14', t => t.is(app.getAlternative(14, exampleTree()), 'aegiklm'));
test('getAlternative 20', t => t.is(app.getAlternative(20, exampleTree()), 'aejklm'));
test.skip('getAlternative 26', t => t.is(app.getAlternative(26, exampleTree()), 'aqr'));
test('getAlternative 999 throw', t => t.throws(() => app.getAlternative(999, exampleTree())));
