import test from 'ava';
import * as app from './dictionary.mjs';

test('get dictionary length', t => {
	const dictionaryString = '(a|b|c)d(e|f|g)';
	const dico = new app.Dictionary(dictionaryString);
	t.is(dico.length, 81);
});
test('get dictionary extraLength', t => {
	const dictionaryString = '(a|b|c)d(e|f|g)';
	const dico = new app.Dictionary(dictionaryString);
	t.true(dico.extraLength >= 81);
});
