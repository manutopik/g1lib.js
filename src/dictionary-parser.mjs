import * as tree from './dictionary-tree.mjs';

export function parse(dictionaryString) {
	const allLines = dictionaryString.split('\n');
	const parsedLines = parseAllLines(allLines);
	return combineUnspecified2IdSecPwd(parsedLines);
}

function parseAllLines(lines) {
	return lines;
}

function parseLine(theLine, allLines) {
	return theLine;
}

function combineUnspecified2IdSecPwd(lines) {
	const idSecPwdLines = [];
	const unspecifiedLines = [];
	lines.forEach(line => line.includes('@@@@') ? idSecPwdLines.push(line) : unspecifiedLines.push(line));
	if (unspecifiedLines.length) idSecPwdLines.push(`${mergeLines(unspecifiedLines)}@@@@${mergeLines(unspecifiedLines)}`)
	return mergeLines(idSecPwdLines);
}

function mergeLines(lines) {
	if (!lines.length) return '';
	lines = lines.filter(l => l.length);
	if (lines.length === 1) return lines[0];
	return `(${lines.join('|')})`;
}
