import test from 'ava';
import * as app from './gva-client.mjs';

// TODO: handle GVASUB in gva-ws-client.mjs
// subscription{newBlocks{number}}

test('gva real server request', async t => {
	const hosts = ['https://g1.librelois.fr/gva'];
	const query = '{"query":"query($pubkey: PkOrScriptGva!) { balance(script: $pubkey) { amount }}","variables":{"pubkey":"2sZF6j2PkxBDNAqUde7Dgo5x3crkerZpQ4rBqqJGn8QT"}}';
	// Unused result example : `{ "data": { "balance": { "amount": 0 } } }`

	const client = new app.GvaClient(hosts);
	const result = await client.query(query);

	t.true(result.data.balance.amount > 0);
});
