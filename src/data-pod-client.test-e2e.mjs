import test from 'ava';
import * as app from './data-pod-client.mjs';

test('data-pod-client real server request', async t => {
	const hosts = ['https://g1.data.e-is.pro/'];
	const query = 'user/profile/2sZF6j2PkxBDNAqUde7Dgo5x3crkerZpQ4rBqqJGn8QT?&_source=title';
	const expectedResult = JSON.parse(`{
		"_index":"user","_type":"profile","_id":"2sZF6j2PkxBDNAqUde7Dgo5x3crkerZpQ4rBqqJGn8QT","_version":11,
		"found":true,
		"_source":{"title":"[1000i100] Millicent BILLETTE"}
	}`);

	const client = new app.DataPodClient(hosts);
	const result = await client.query(query);

	t.is(result._source.title, expectedResult._source.title);
});
