import {fetch} from './context-dependant/generics.mjs';

export function GvaClient(hosts) {
	const self = this;
	self.hosts = hosts;
	self.query = queryStr => query(self, queryStr);
	return self;
}

async function query(self, queryStr) {
	return (await fetch(self.hosts[0], {method: 'POST', body: queryStr})).json();
}
