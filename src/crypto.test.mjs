import test from 'ava';
import * as app from './crypto.mjs';

const idSec = 'a';
const mdp = 'b';
// Base58
const pubKey = 'AoxVA41dGL2s4ogMNdbCw3FFYjFo5FPK36LuiW1tjGbG';
const secretKey = '3ZsmZhnRv137dS1s7Q3jFGKLTDyhkwguPHfnWBxzDCTTHKWGnYw9zBk3gcCUJCc72TEUuyzM7cqpo7c5LYhs1Qtv';
const seed = '9eADqX8V6VcPdJCHCVYiE1Vnift9nFNrvr9aTaXA5RJc';
const unsignedDocument = `
Version: 10
Type: Identity
Currency: duniter_unit_test_currency
Issuer: AoxVA41dGL2s4ogMNdbCw3FFYjFo5FPK36LuiW1tjGbG
UniqueID: tic
Timestamp: 0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855`;
const signedDocument = `Version: 10
Type: Identity
Currency: duniter_unit_test_currency
Issuer: AoxVA41dGL2s4ogMNdbCw3FFYjFo5FPK36LuiW1tjGbG
UniqueID: tic
Timestamp: 0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855
8BZ2NE/d4YO2rOFpJFZdEYTIoSL4uSX9zo6tacpHBcCIlSlhkHTIHbSJNuzLl9uVBIO0skI7NZPxEYXIJGQYBg==`;

test('signDocument', async t => t.is(await app.signDocument(unsignedDocument, secretKey), signedDocument));
test('b64 sign string', async t => t.is(await app.sign(unsignedDocument, secretKey), 'G6ma6n+rpJ+7PPsUuJjNtzfGQqLWNqRToSlurt8vjrHa7G0tm7oVObJjQBWGMK0zs4/25xXidT19RrfZqWV/DQ=='));
test('b58 sign string', async t => t.is(await app.sign(unsignedDocument, secretKey, 'b58'), 'Z5W7C7ZUwTRPRTCrXtRqmY4WakgEXAkPBDiVEVxoTwrCSvDrup19sENe9tfFNMFKL9ZFdiFWJCSJ2ftgeeDzFsz'));
test('raw sign string', async t => t.is((await app.sign(unsignedDocument, secretKey, 'raw'))[0], 27));
test('array sign string', async t => t.is((await app.sign(unsignedDocument, secretKey, 'Array'))[0], 27));
test('uint8array sign string', async t => t.is((await app.sign(unsignedDocument, secretKey, 'uint8array'))[0], 27));
test('sign throw for bad output format', async t => t.throwsAsync(() => app.sign(unsignedDocument, secretKey, 'whattt ?')));

test("is a pubkey", (t) => t.is(app.isPubkey(pubKey), true));
test('b58 should decode/encode well', t => t.is(app.b58.encode(app.b58.decode(pubKey)), pubKey));
test('b58 on pubKey with leading 1', t => t.is(app.b58.encode(app.b58.decode('12BjyvjoAf5qik7R8TKDJAHJugsX23YgJGi2LmBUv2nx')), '12BjyvjoAf5qik7R8TKDJAHJugsX23YgJGi2LmBUv2nx'));
test('b58 on pubKey without leading 1', t => t.is(app.b58.encode(app.b58.decode('2BjyvjoAf5qik7R8TKDJAHJugsX23YgJGi2LmBUv2nx')), '2BjyvjoAf5qik7R8TKDJAHJugsX23YgJGi2LmBUv2nx'));

test('saltPass2seed should convert salt & password to seed with scrypt', async t => {
	t.is(app.b58.encode(await app.saltPass2seed(idSec, mdp)), seed);
});
test('seed2keyPair should generate public and private key nacl/sodium way.', async t => {
	const rawSeed = app.b58.decode(seed);
	const rawKeyPair = await app.seed2keyPair(rawSeed);
	t.is(app.b58.encode(rawKeyPair.publicKey), pubKey);
	t.is(app.b58.encode(rawKeyPair.secretKey), secretKey);
});
test('idSecPass2cleanKeys should output clean base58 keys and seed', async t => {
	const r = await app.idSecPass2cleanKeys(idSec, mdp);
	t.is(r.publicKey, pubKey);
	t.is(r.secretKey, secretKey);
	t.is(r.seed, seed);
	t.is(r.idSec, idSec);
	t.is(r.password, mdp);
});
test('pubKey2shortKey match Mutu…Yr41:5cq', t => {
	const pubKey = 'Mutu112HLfUbgVy4obN9kp3MnnDGShke88wrZr2Yr41';
	const shortKey = 'Mutu…Yr41:5cq';
	t.is(app.pubKey2shortKey(pubKey), shortKey);
});
test('pubKey2shortKey match RML1…zvSY:3k4', t => {
	const pubKey = 'RML12butzV3xZmkWnNAmRwuepKPYvzQ4euHwhHhzvSY';
	const shortKey = 'RML1…zvSY:3k4';
	t.is(app.pubKey2shortKey(pubKey), shortKey);
});
test('pubKey2checksum RML12butz : 3k4', t => t.is(app.pubKey2checksum('RML12butzV3xZmkWnNAmRwuepKPYvzQ4euHwhHhzvSY'), '3k4'));
test('pubKey2checksum 12Bj : 8pQ', t => t.is(app.pubKey2checksum('12BjyvjoAf5qik7R8TKDJAHJugsX23YgJGi2LmBUv2nx'), '8pQ'));
test('pubKey2checksum 2Bjy : 8pQ', t => t.is(app.pubKey2checksum('2BjyvjoAf5qik7R8TKDJAHJugsX23YgJGi2LmBUv2nx'), '8pQ'));
test('pubKey2checksum ascii 2Bjy : 5vi', t => t.is(app.pubKey2checksum('2BjyvjoAf5qik7R8TKDJAHJugsX23YgJGi2LmBUv2nx', true), '5vi'));
test('pubKey2checksum 1111 : 3ud', t => t.is(app.pubKey2checksum('11111111111111111111111111111111'), '3ud'));
test('pubKey2checksum "" : 3ud', t => t.is(app.pubKey2checksum(''), '3ud'));
test('pubKey2checksum 1pubKey542 : MLT', t => t.is(app.pubKey2checksum('1pubKey542'), 'MLT'));
test('pubKey2checksum pubKey542 : MLT', t => t.is(app.pubKey2checksum('pubKey542'), 'MLT'));
test('pubKey2checksum ascii 1111111111111111111111111pubKey542 : MLT', t => t.is(app.pubKey2checksum('1111111111111111111111111pubKey542', true), 'MLT'));
test('pubKey2checksum ascii 1pubKey542 : DSs', t => t.is(app.pubKey2checksum('1pubKey542', true), 'DSs'));
test('pubKey2checksum ascii pubKey542 : DEE', t => t.is(app.pubKey2checksum('pubKey542', true), 'DEE'));
test('pubKey2checksum checksumWithLeadingZero 1pubKey542 : 1ML', t => t.is(app.pubKey2checksum('pubKey542', false, true), '1ML'));

test('checkKey pubKey542:1ML', t => t.true(app.checkKey('pubKey542:1ML')));
test('checkKey pubKey542:MLT', t => t.true(app.checkKey('pubKey542:MLT')));
test('checkKey pubKey542:DEE', t => t.true(app.checkKey('pubKey542:DEE')));

test('checkKey 11111111111111111111111pubKey49311:4Ru', t => t.true(app.checkKey('11111111111111111111111pubKey49311:4Ru')));
test('checkKey 11111111111111111111111pubKey49311:14R', t => t.true(app.checkKey('11111111111111111111111pubKey49311:14R')));
test('checkKey 111pubKey49311:14R', t => t.true(app.checkKey('111pubKey49311:14R')));
test('checkKey 11pubKey49311:14R', t => t.true(app.checkKey('11pubKey49311:14R')));
test('checkKey 1pubKey49311:14R', t => t.true(app.checkKey('1pubKey49311:14R')));
test('checkKey pubKey49311:14R', t => t.true(app.checkKey('pubKey49311:14R')));
test('checkKey pubKey49311:4Ru', t => t.true(app.checkKey('pubKey49311:4Ru')));
test('checkKey pubKey49311:12p', t => t.true(app.checkKey('pubKey49311:12p')));
test('checkKey pubKey49311:2p7', t => t.true(app.checkKey('pubKey49311:2p7')));
test('checkKey false 11111111111111111111111pubKey49311:12p', t => t.throws(() => app.checkKey('11111111111111111111111pubKey49311:12p')));
test('checkKey false 11111111111111111111111pubKey49311:2p7', t => t.throws(() => app.checkKey('11111111111111111111111pubKey49311:2p7')));
test('checkKey false pubKey49311:111', t => t.throws(() => app.checkKey('pubKey49311:111')));

test('checkKey false 0pubKey49311:any', t => t.throws(() => app.checkKey('0pubKey49311:any')));

test('pubKey2checksum 11111111111111111111111pubKey49311 : 4Ru', t => t.is(app.pubKey2checksum('11111111111111111111111pubKey49311'), '4Ru'));
test('pubKey2checksum pubKey49311 : 4Ru', t => t.is(app.pubKey2checksum('pubKey49311'), '4Ru'));

test('pubKey2checksum de Nd5...21o:3Q3', t => t.is(app.pubKey2checksum('Nd5kTAZmFDuKoi1mAZkZERenV6efyYyyLoHMTe721o'), '3Q3'));
test('pubKey2checksum simpleSha de Nd5...21o:3Q3', t => t.is(app.pubKey2checksum('Nd5kTAZmFDuKoi1mAZkZERenV6efyYyyLoHMTe721o', false, false, false), 'FCd'));
test('checkKey accept simpleSha pub664777:4fv', t => t.true(app.checkKey('pub664777:4fv')));
test('checkKey accept simpleSha pub664777:14f', t => t.true(app.checkKey('pub664777:14f')));
test('checkKey accept simpleSha pub664777:2u3', t => t.true(app.checkKey('pub664777:2u3')));
test('checkKey accept simpleSha pub664777:12u', t => t.true(app.checkKey('pub664777:12u')));

test('pubKey2checksum simpleSha viewDependant Nd5...21o', t => t.is(app.pubKey2checksum('Nd5kTAZmFDuKoi1mAZkZERenV6efyYyyLoHMTe721o', true, false, false), '7G5'));
test('pubKey2checksum simpleSha checksumWithLeadingZero Nd5...21o', t => t.is(app.pubKey2checksum('Nd5kTAZmFDuKoi1mAZkZERenV6efyYyyLoHMTe721o', false, true, false), 'FCd'));
test('pubKey2checksum simpleSha viewDependant checksumWithLeadingZero Nd5...21o', t => t.is(app.pubKey2checksum('Nd5kTAZmFDuKoi1mAZkZERenV6efyYyyLoHMTe721o', true, true, false), '7G5'));
