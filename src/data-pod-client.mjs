import {fetch} from './context-dependant/generics.mjs';
export const mockableDeps = {
	fetch
};
export function DataPodClient(hosts) {
	const self = this;
	self.hosts = hosts;
	self.query = queryStr => query(self, queryStr);
	return self;
}

async function query(self, queryStr) {
	return (await mockableDeps.fetch(self.hosts[0] + queryStr)).json();
}
