export class Dictionary {
	constructor(dictionaryString) {
		this.properties = initProperties(dictionaryString);
		incorporate(this, this.properties);
	}
}

export function initProperties(dictionaryString) {
	if (!dictionaryString) return {};
	const properties = {
		length: 81,
		extraLength: 90
	};
	return properties;
}

export default Dictionary;

function incorporate(target, toAdd) {
	for (const key in toAdd) {
		target[key] = toAdd[key];
	}
}
