// Alt deps :  import {generate_keypair} from "ecma-nacl/build/lib/signing/sign.js";
// Alt deps :  import scrypt from "ecma-nacl/build/lib/scrypt/scrypt.js";
import scrypt from '../generated/vendors/scrypt.mjs';
import nacl from '../generated/vendors/nacl.mjs';
import sha from '../node_modules/js-sha256/src/sha256.mjs';
// Alt import * as ed25519 from '../node_modules/noble-ed25519/index.mjs';
import {b58, b64} from './basex.mjs';

export {b58, b64};

const sha256 = sha();

const generateKeypair = nacl.sign.keyPair.fromSeed;

export const isPubkey = (value) => /^[A-HJ-NP-Za-km-z1-9]{42,45}$/.test(value)

export async function idSecPass2rawAll(idSec, pass) {
	const rawSeed = await saltPass2seed(idSec, pass);
	const keyPair = await seed2keyPair(rawSeed);
	keyPair.seed = rawSeed;
	keyPair.pubKey = keyPair.publicKey;
	return keyPair;
}

export function raw2b58(raws) {
	const result = {};
	for (const r in raws) result[r] = b58.encode(raws[r]);
	return result;
}

export async function idSecPass2cleanKeys(idSec, pass) {
	const raw = await idSecPass2rawAll(idSec, pass);
	return Object.assign(raw2b58(raw), {idSec, password: pass});
}

export function seed2keyPair(seed) {
	return generateKeypair(seed);
}

/* Noble edition
export async function seed2keyPair(rawSeed) {
	const pubKey = await ed25519.getPublicKey(rawSeed);
	console.log('pubKey',pubKey);
	const naclLikePrivateKey = new Uint8Array(64);
	naclLikePrivateKey.set(rawSeed);
	naclLikePrivateKey.set(pubKey,32);
	return {
		publicKey:pubKey,
		secretKey:naclLikePrivateKey
	};
}
*/
export async function saltPass2seed(idSec, pass) {
	const options = {
		logN: 12,
		r: 16,
		p: 1,
		// Default: dkLen: 32,
		encoding: 'binary'
	};
	return scrypt(pass.normalize('NFKC'), idSec.normalize('NFKC'), options);
}

export function pubKey2shortKey(pubKey) {
	const pubKeyBegin = pubKey.substr(0, 4);
	const pubKeyEnd = pubKey.substr(-4, 4);
	const checksum = pubKey2checksum(pubKey);
	return `${pubKeyBegin}…${pubKeyEnd}:${checksum}`;
}

export function pubKey2checksum(b58pubKey, b58viewDependant = false, checksumWithLeadingZero = false, doubleSha256 = true) {
	const binPubKey = b58viewDependant ? b58.decode(b58pubKey) : b58pubKey2bin(b58pubKey);
	let hash = sha256.digest(binPubKey);
	if (doubleSha256) hash = sha256.digest(hash);
	if (!checksumWithLeadingZero) {
		const shorterHash = sliceInitialsZero(hash);
		return b58.encode(shorterHash).substr(0, 3);
	}

	return b58.encode(hash).substr(0, 3);
}

export function sliceInitialsZero(array) {
	let zero = 0;
	while (array[zero] === 0) zero++;
	return array.slice(zero);
}

export function b58pubKey2bin(b58pubKey) {
	const binPubKey = new Uint8Array(32);
	const decoded = b58.decode(b58pubKey);
	binPubKey.set(decoded, 32 - decoded.length);
	return binPubKey;
}

export function b58secretKey2bin(b58secretKey) {
	const binSecretKey = new Uint8Array(64);
	const decoded = b58.decode(b58secretKey);
	binSecretKey.set(decoded, 64 - decoded.length);
	return binSecretKey;
}

export function checkKey(pubKeyWithChecksum) {
	const part = pubKeyWithChecksum.split(':');
	const b58pubKey = part[0];
	const checkSum = part[1];
	if (pubKey2checksum(b58pubKey) === checkSum) return true;
	if (pubKey2checksum(b58pubKey, true) === checkSum) return true;
	if (pubKey2checksum(b58pubKey, false, true) === checkSum) return true;
	if (pubKey2checksum(b58pubKey, true, true) === checkSum) return true;
	if (pubKey2checksum(b58pubKey, false, false, false) === checkSum) return true;
	if (pubKey2checksum(b58pubKey, true, false, false) === checkSum) return true;
	if (pubKey2checksum(b58pubKey, false, true, false) === checkSum) return true;
	if (pubKey2checksum(b58pubKey, true, true, false) === checkSum) return true;
	throw new Error('Bad checksum');
}

export async function signDocument(unsignedDocument, secretKey) {
	const signHash = await sign(unsignedDocument.trim() + '\n', secretKey);
	return `${unsignedDocument.trim()}\n${signHash}`;
}

export async function sign(str, secretKey, outputFormat = 'b64') {
	const encoder = new TextEncoder();
	const raw = await rawSign(encoder.encode(str), b58secretKey2bin(secretKey).slice(0, 32));
	switch (outputFormat.toLocaleLowerCase()) {
		case 'raw':
		case 'array':
		case 'uint8array':
			return raw;
		case 'b64':
			return b64.encode(raw);
		case 'b58':
			return b58.encode(raw);
		default:
			throw new Error(`OutputFormat ${outputFormat} not handled.`);
	}
}

export async function rawSign(uint8Array, rawSeed) {
	const keys = await seed2keyPair(rawSeed);
	return nacl.sign.detached(uint8Array, keys.secretKey);
}
