import test from 'ava';
import * as app from './multi-node-layer.mjs';

function sleep(ms){
	return new Promise((resolve)=>setTimeout(resolve,ms));
}

test('pingTracker give time elapsed', async t => {
	const mockedFunc = async (first,second)=>{await sleep(second);return first};
	const firstParameter = 'finalResult';
	const secondParameter = 11;
	const toCall = [mockedFunc,firstParameter,secondParameter];
	const timeout = 1000;
	const result = await app.timeTrack(toCall,timeout);
	t.is(result.result,firstParameter);
	t.is(result.params[1],firstParameter);
	t.regex(result.ms.toString(),/1[0-9]/);
});
test('pingTracker can timeout', async t => {
	const moreThanTimeout = 15
	const mockedFunc = async (first)=>{await sleep(moreThanTimeout);return first};
	const toCall = [mockedFunc,'param'];
	const timeout = 10;
	const result = await app.timeTrack(toCall,timeout);
	t.is(result.params[1],'param');
	t.is(typeof result.result,'undefined');
	t.is(result.ms,10);
	t.is(result.timeout,true);
});

/**
 * prend en paramètre un objet qui implémente le réseau adapté (gva-client par exemple)
 * lors d'une requête, la fait passer par le client.
 * si la requête échoue, suis-je en ligne ? si non : throw hors ligne, si oui, désactive la cible et lance un scan réseau
 * si une requête est demandé avec un réglage de sécurité > 1 demande à l'implémentaiton au moins n résultats égaux avant de retourner la réponse.
 * si des résultats sont différents, demande à l'implémentation de trancher (peut répondre ou throw info insuffisante).
 * tant que les résultats sont insuffisant, demande à plus de noeuds (à chaque nouvelle réponse, demande s'il est possible de trancher).
 * si plus de noeud connu qu'un seuil, retire les désactivé excédentaires, les plus anciens, et arrête le scan.
 *
 * si un requête est demandée avec un réglage orienté vitesse, demande à n(défaut 2) noeuds la réponse, et répond dès la première (sauf réglage sécurité en plus)
 * ps : classe les noeuds par ping pour reuqêter en priorité les plus réactifs.
 *
 *
 * scan réseau :
 * demande à chacun des noeuds connu, dans l'ordre, leur noeuds voisin et les ajoutes (et les requêtes à leur tour juqu'à ce qu'aucun nouveau noeuds ne soient trouvé).
 * si le nombre max est atteint, arrête le scan.
 *
 * chaque noeud requêté fourni sa liste de voisins et est associé à :
 * son ping (ou timeout)
 * le timestamp de dernier ping.
 *
 *
 * à chaque requête demandé au module, quel que soit l'état d'avancement du scan, une liste trié par ping des noeud est exporté pour être utilisé pour le requêtage.
 *
 */
