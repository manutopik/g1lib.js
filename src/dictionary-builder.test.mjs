import test from 'ava';
import * as app from './dictionary-builder.mjs';

test.beforeEach(app.resetCache);
test('add no accents variant', t => {
	t.deepEqual(app.noAccentVariant('Ǧ1'), ['Ǧ1', 'G1']);
});
test('add case variants', t => {
	t.deepEqual(app.casesVariants('moT'), ['moT', 'mot', 'Mot', 'MOT']);
});
test('add multi word case variants', t => {
	t.deepEqual(app.casesVariants('autre mot'), ['autre mot', 'Autre Mot', 'AUTRE MOT']);
});
test('regLikeVariants remove ref:: lines', t => {
	t.deepEqual(app.regLikeVariants('ref::truc'), []);
});
// TODO: handle ref infinite loop by returning §infiniteRecursion§
test('regLikeVariants handle <ref>', t => {
	t.deepEqual(app.regLikeVariants('<ref> <ref>', ['ref::truc', 'ref::bidule', '<ref> <ref>']), ['truc truc', 'bidule truc', 'truc bidule', 'bidule bidule']);
	t.deepEqual(app.regLikeVariants('<ref> <ref>', ['ref::(truc|bidule)', '<ref> <ref>']), ['truc truc', 'bidule truc', 'truc bidule', 'bidule bidule']);
});
test('regLikeVariants handle =ref>', t => {
	t.deepEqual(app.regLikeVariants('=ref> =ref>', ['ref::truc', 'ref::bidule', '=ref> =ref>']), ['truc truc', 'bidule bidule']);
	t.deepEqual(app.regLikeVariants('=ref> =ref>', ['ref::(truc|bidule)', '=ref> =ref>']), ['truc truc', 'bidule bidule']);
});
test('regLikeVariants handle multiple =ref>', t => {
	t.deepEqual(
		app.regLikeVariants('=ref> =ref2> =ref> =ref2>', ['ref::(truc|bidule)', 'ref2::(machin|chose)', '=ref> =ref>']),
		['truc machin truc machin', 'bidule machin bidule machin', 'truc chose truc chose', 'bidule chose bidule chose']);
});
test('regLikeVariants handle (this|that)', t => {
	t.deepEqual(app.regLikeVariants('(this|that)'), ['this', 'that']);
});
test('regLikeVariants handle [ -_]', t => {
	t.deepEqual(app.regLikeVariants('[ -_]'), [' ', '-', '_']);
});
test('regLikeVariants handle [a-f]', t => {
	t.deepEqual(app.regLikeVariants('[a-f]'), ['a', 'b', 'c', 'd', 'e', 'f']);
	t.deepEqual(app.regLikeVariants('[7-9]'), ['7', '8', '9']);
	t.deepEqual(app.regLikeVariants('[C-F]'), ['C', 'D', 'E', 'F']);
});
test('regLikeVariants handle [a-c-]', t => {
	t.deepEqual(app.regLikeVariants('[a-c-]'), ['a', 'b', 'c', '-']);
});
test('regLikeVariants handle {qty}', t => {
	t.deepEqual(app.regLikeVariants('a{5}'), ['aaaaa']);
});
test('regLikeVariants handle {min,max}', t => {
	t.deepEqual(app.regLikeVariants('b{3,5}'), ['bbb', 'bbbb', 'bbbbb']);
});
test('regLikeVariants handle (string){qty}', t => {
	t.deepEqual(app.regLikeVariants(`c'est (toto|tata){0,2}`), [`c'est `, `c'est toto`, `c'est totototo`, `c'est tata`, `c'est tatatoto`, `c'est tototata`, `c'est tatatata`]);
});
test.serial('regLikeVariants handle nested -([a-f]|<ref>){0,1}', t => {
	t.deepEqual(
		app.regLikeVariants('-([B-D]|<ref>){0,1}', ['ref::plop']),
		['-', '-B', '-plop', '-C', '-D']);
});
test('regLikeVariants handle plop:\\:', t => {
	t.deepEqual(app.regLikeVariants('plop:\\:ici'), ['plop::ici']);
	t.deepEqual(app.regLikeVariants('plop\\::ici'), ['plop::ici']);
	t.deepEqual(app.regLikeVariants('plop::ici'), []);
});
test('regLikeVariants handle [\\]*]', t => {
	t.deepEqual(app.regLikeVariants('[\\]*]'), [']', '*']);
});
test('regLikeVariants handle escaping common chr \\a', t => {
	t.deepEqual(app.regLikeVariants('\\a'), ['a']);
});
