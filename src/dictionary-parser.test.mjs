import test from 'ava';
import * as app from './dictionary-parser.mjs';

test('no @@@@ idSec password separator ? implicit add it', t => {
	t.is(app.parse('a'), 'a@@@@a');
});
test('@@@@ present, do not add it', t => {
	t.is(app.parse('a@@@@b'), 'a@@@@b');
});
test('no @@@@ on each line ? combine no @@@@ lines', t => {
	t.is(app.parse(`
a@@@@(b|c)
(d|e)
(f|g)
h@@@@(i|j)
`), '(a@@@@(b|c)|h@@@@(i|j)|((d|e)|(f|g))@@@@((d|e)|(f|g)))');
});
