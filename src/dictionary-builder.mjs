import latinize from '../node_modules/latinize-to-ascii/latinize.mjs';

export {dedup, noAccentVariant, casesVariants, regLikeVariants, resetCache};

function dedup(array) {
	const result = {};
	array.forEach(v => result[v] = v); // eslint-disable-line no-return-assign
	return Object.keys(result);
}

function noAccentVariant(string) {
	return dedup([string, latinize(string)]);
}

function casesVariants(string) {
	return dedup([
		string,
		string.toLowerCase(),
		string.split(' ').map(str => str.charAt(0).toUpperCase() + str.slice(1).toLowerCase()).join(' '),
		string.toUpperCase()
	]);
}

function regLikeVariants(theString, allStrings = []) {
	return _regLikeVariants(escape2utfSpecial(theString), allStrings.map(escape2utfSpecial)).map(utfSpecial2unEscaped);
}

function swapKeyValue(object) {
	const result = {};
	for (const key in object) {
		result[object[key]] = key;
	}

	return result;
}

const specialMap = {
	'(': String.fromCharCode(0x01),
	')': String.fromCharCode(0x02),
	'|': String.fromCharCode(0x03),
	'{': String.fromCharCode(0x04),
	'}': String.fromCharCode(0x05),
	',': String.fromCharCode(0x06),
	'[': String.fromCharCode(0x07),
	']': String.fromCharCode(0x08),
	'-': String.fromCharCode(0x09),
	'<': String.fromCharCode(0x0a),
	'>': String.fromCharCode(0x0b),
	':': String.fromCharCode(0x0c),
	'=': String.fromCharCode(0x0d)
};
const revertSpecial = swapKeyValue(specialMap);

function escape2utfSpecial(str) {
	return str.replace(/\\(.)/g, (a, chr) => specialMap[chr] ? specialMap[chr] : chr);
}

function utfSpecial2unEscaped(str) {
	return str.split('').map(chr => revertSpecial[chr] ? revertSpecial[chr] : chr).join('');
}

function bracketsHandler(theString) {
	// Handle []
	const lower = 'abcdefghijklmnopqrstuvwxyz';
	const upper = lower.toUpperCase();
	const number = '0123456789';
	theString = theString.replace(/(\[[^\]]*)([a-z]-[a-z])([^\]]*])/g, (osef, before, chrs, after) => before + lower.slice(lower.indexOf(chrs.split('-')[0]), lower.indexOf(chrs.split('-')[1]) + 1) + after);
	theString = theString.replace(/(\[[^\]]*)([A-Z]-[A-Z])([^\]]*])/g, (osef, before, chrs, after) => before + upper.slice(upper.indexOf(chrs.split('-')[0]), upper.indexOf(chrs.split('-')[1]) + 1) + after);
	theString = theString.replace(/(\[[^\]]*)([0-9]-[0-9])([^\]]*])/g, (osef, before, chrs, after) => before + number.slice(number.indexOf(chrs.split('-')[0]), number.indexOf(chrs.split('-')[1]) + 1) + after); // eslint-disable-line unicorn/better-regex
	theString = theString.replace(/\[([^\]]+)]/g, (osef, chrs) => `(${chrs.split('').join('|')})`);
	return theString;
}

function resetCache() {
	cache();
}

function cache(key, func, ...args) {
	/* Init */ if (!cache.cached) cache.cached = {};
	/* Reset cache */ if (arguments.length === 0) return cache.cached = {}; // eslint-disable-line no-return-assign
	/* Compute and cache */ if (typeof cache.cached[key] === 'undefined') cache.cached[key] = func(...args);
	/* Answer from cache */ return cache.cached[key];
}

function labelExpressions(str) {
	return str.slice(str.indexOf('::') + 2);
}

function computedLabel(label, allStrings) {
	return cache(`label::${label}`, _computedLabel, label, allStrings);
}

function _computedLabel(label, allStrings) {
	const matchingLabel = allStrings.filter(str => str.trim().indexOf(`${label}::`) === 0);
	return flatten(matchingLabel.map(str => _regLikeVariants(labelExpressions(str)), allStrings));
}

function refHandler(theString, allStrings) {
	// Handle <ref>
	theString = theString.replace(/<([^>]+)>/g, (osef, ref) => `(${computedLabel(ref, allStrings).join('|')})`);
	return theString;
}

function syncRefHandler(theString, allStrings) {
	// Handle =ref>
	const syncRef = dedup(theString.match(/=[^>]+>/g) || []).map(str => str.substring(1, str.length - 1));
	let results = [theString];
	for (const ref of syncRef) {
		const variantes = computedLabel(ref, allStrings);
		const tmpRes = [];
		for (const v of variantes) {
			for (const r of results) {
				tmpRes.push(r.replace(new RegExp(`=${ref}>`, 'g'), v));
			}
		}

		results = tmpRes;
	}

	return results;
}

function qtyHandlerReplaceCallback(all, chr, qty) {
	const mm = qty.split(',').map(n => n.trim() * 1); // eslint-disable-line no-implicit-coercion
	const min = mm[0];
	const max = (mm.length === 2) ? mm[1] : min;
	const result = [];
	for (let i = min; i <= max; i++) result.push(new Array(i + 1).join(chr)); // eslint-disable-line unicorn/no-new-array
	return `(${result.join('|')})`;
}

function qtyHandler(theString) {
	// Handle {qty} and {min,max}
	theString = theString.replace(/([^)]){([^}]+)}/g, qtyHandlerReplaceCallback);
	theString = theString.replace(/^(.*)\(([^)]*)\){([^}]+)}(.*)$/, (all, before, choices, qty, after) => before + qtyHandlerReplaceCallback('', `(${choices})`, qty) + after); // eslint-disable-line max-params
	return theString;
}

function optionsHandler(theString, allStrings) {
	// Handle (|)
	let multiString = theString.replace(/^(.*)\(([^)]*)\)(.*)$/, (all, before, choices, after) => choices.split('|').map(c => before + c + after).join('=$##$=')).split('=$##$=');
	multiString = [].concat(...multiString.map(str => (str.includes('(')) ? _regLikeVariants(str, allStrings) : str));
	return dedup(multiString);
}

function _regLikeVariants(theString, allStrings) {
	if (theString.indexOf('::') > 0) return []; // Remove label definition lines.
	theString = bracketsHandler(theString); // Handle []
	theString = refHandler(theString, allStrings);// Handle <ref> and ref::
	theString = qtyHandler(theString); // Handle {qty} and {min,max}
	const strings = optionsHandler(theString, allStrings); // Handle (|)
	return flatten(strings.map(string => syncRefHandler(string, allStrings)));// Handle =ref> and ref::
}

function flatten(arrayOfArray) {
	return dedup([].concat(...arrayOfArray));
}
