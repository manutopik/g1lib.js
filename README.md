[![pipeline status](https://git.duniter.org/libs/g1lib.js/badges/main/pipeline.svg)](https://git.duniter.org/libs/g1lib.js/-/pipelines)
[![coverage report](https://git.duniter.org/libs/g1lib.js/badges/main/coverage.svg)](https://libs.duniter.io/g1lib.js/coverage/)

[![duplication](https://libs.duniter.io/g1lib.js/jscpd-badge.svg)](https://libs.duniter.io/g1lib.js/jscpd/)
[![maintainability](https://libs.duniter.io/g1lib.js/worst-maintainability.svg)](https://libs.duniter.io/g1lib.js/maintainability/)

[![release](https://img.shields.io/npm/v/g1lib.svg)](https://www.npmjs.com/package/g1lib)
[![usage as download](https://img.shields.io/npm/dy/g1lib.svg)](https://www.npmjs.com/package/g1lib)

EN | [FR](README.fr.md)

Full english translation not released yet, see up-to-date FR version.
(Pas de version anglaise pour l'instant, consultez la version française).

# G1lib.js

An ubiquitous static javascript lib for Ǧ1 / Duniter ecosystem with reliability in mind.

- [x] Unit tested
- [x] Tracked quality metrics
- [x] Static package with no-dependency
- [x] import only what you need*

*modern esm import/export & high modularity improve tree-shaking

## Usage

Install nodejs then in cli :
```
npm install --save g1lib
```
In js :
```
import * as g1lib from "g1lib"
```
Other packaging use-case ? (asm, cjs...), [ask for it](https://framagit.org/g1/g1lib.js/-/issues)

For user privacy, CDN should not be used in production.
For quick prototyping without npm : https://g1.frama.io/g1lib.js/dist/all.mjs

## Contribute

Ask for features or fix in [issues](https://framagit.org/g1/g1lib.js/-/issues).

Merge-request welcome.
Quality requirement :
- follow opinionated g1lib.js choices
- covered by unit-tests
- follow Clean Code good practices ISBN :  978-0-1323-5088-4

If you need help to reach quality requirement, ask it (in [issue](https://framagit.org/g1/g1lib.js/-/issues) or merge-request).

## Funding

Ǧ1 and common fiat are welcome (crypto currencies are welcome to but i don't allready have wallet).

G1Lib.js author and maintainer is [1000i100] Millicent Billette.

To send me money, all you need is at the bottom of this document : [CGV](https://1forma-tic.fr/#CGV).
