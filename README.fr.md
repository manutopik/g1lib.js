
[![pipeline status](https://git.duniter.org/libs/g1lib.js/badges/main/pipeline.svg)](https://git.duniter.org/libs/g1lib.js/-/pipelines)
[![coverage report](https://git.duniter.org/libs/g1lib.js/badges/main/coverage.svg)](https://libs.duniter.io/g1lib.js/coverage/)

[![duplication](https://libs.duniter.io/g1lib.js/jscpd-badge.svg)](https://libs.duniter.io/g1lib.js/jscpd/)
[![maintainability](https://libs.duniter.io/g1lib.js/worst-maintainability.svg)](https://libs.duniter.io/g1lib.js/maintainability/)

[![release](https://img.shields.io/npm/v/g1lib.svg)](https://www.npmjs.com/package/g1lib)
[![usage as download](https://img.shields.io/npm/dy/g1lib.svg)](https://www.npmjs.com/package/g1lib)

[EN](README.md) | FR

# G1lib.js
Une lib javascript (avec éventuellement du web assembly / wasm issue de rust) pour développer des clients Ǧ1 modulaire fiable et maintenable.
L'idée est de favoriser le développement d'outils serverless ou assimilé, dont l'essentiel du fonctionnement à lieu coté client (dans le navigateur).
Transformable en application mobile ou extension navigateur grace aux technos de PWA (Progressive Web App).

G1lib.js se veux utilisable aussi bien dans le navigateur que coté serveur (ubiquitous javascript),
sans dépendances (sauf lib de crypto actuellement), packagé sans aucune dépendance (static package) pour en faciliter l'usage.

G1lib.js est couvert par des tests unitaires avec une couverture de test aussi proche de 100% que possible.

G1lib.js est une bibliothèque de code dédié à l'écosystème Ǧ1 / Duniter.
Elle est destinée à être inclue dans des clients web, PWA ou ligne de commande (cli).
Sa responsabilité n'est pas d'être utilisable directement en tant que client mais d'être utilisé par ces derniers
pour mutualiser du code métier fiable, maintenable, facile à auditer et facilement accessible à la contribution.

## Usage

Installez nodejs puis en cli :
```
npm install --save g1lib
```
En js :
```
import * as g1lib from "g1lib"
```
Si vous souhaitez la lib en asm, cjs ou autre packaging, [demandez-le](https://framagit.org/g1/g1lib.js/-/issues)

Pour la confidentialité de la navigation des internautes, les CDN publiques sont à proscrire en production.
Pour du prototypage rapide sans npm : https://g1.frama.io/g1lib.js/dist/all.mjs

## Contribuer

Vous pouvez proposer des évolutions sous forme de [ticket](https://framagit.org/g1/g1lib.js/-/issues) aussi bien que des demandes d'aide (en français ou en anglais).

Les merge-request sont bienvenue.
Elles seront acceptées si :
- elles respectent la philosophie de g1lib.js
- elles sont couvertes par des tests unitaires
- leur code respecte les bonnes pratiques décrites dans l'ouvrage [Clean Code / Coder proprement](https://dl.leneveu.fr/public/Coder_Proprement.pdf) ISBN : 978-2-7440-4104-4

Si vous avez besoin d'aide pour respecter les critères d'acceptation, n'hésitez pas à demander (par [ticket](https://framagit.org/g1/g1lib.js/-/issues) ou [email](https://1forma-tic.fr/#contact)).

## Financer le développement

Les dons sont bienvenus en Ǧ1 comme en UNL.

Actuellement L'auteur et mainteneur de G1Lib.js est [1000i100] Millicent Billette.

En fin de [CGV](https://1forma-tic.fr/#CGV) sur mon site pro, vous trouverez le nécessaire pour me soutenir.
