[EN](CHANGELOG.md) | FR

# Changelog | liste des changements
Tous les changements notables de ce projet seront documenté dans ce fichier.

Le format est basé sur [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
et ce projet adhère au [versionnage sémantique](https://semver.org/spec/v2.0.0.html).

## Evolutions probable / Roadmap :
- GraphQL stuff
- @@@@ comme séparateur entre identifiant secret et mdp pour la génération de combinaison à tester (usage principal Gsper)
- supprimer automatiquement le code inutile dans les lib (Tree Shaking)

## [Non-publié/Non-Stabilisé] (par [1000i100])

### Ajouté
- crypto.checkKey(pubKeyWithChecksum)
- crypto.pubKey2checksum(b58pubKey, optionalBool:b58viewDependant, optionalBool:checksumWithoutLeadingZero)

## [Version 3.1.0] - 2021-04-01 (par [1000i100] & [Hugo])
### Ajouté
- génération du [format court d'affichage de pubKey](https://forum.duniter.org/t/format-de-checksum/7616)
	```javascript
	import {pubKey2shortKey} from 'g1lib/crypto.mjs';
	const shortKey = pubKey2shortKey("Mutu112HLfUbgVy4obN9kp3MnnDGShke88wrZr2Yr41");
	// shortKey === "Mutu…Yr41:5cq"
 	```

## [Version 3.0.2] - 2020-12-10 (par [1000i100])
### Ajouté
- minification des modules g1lib
- publication automatisé sur npm à la publication de tag (sous réserve que la CI passe)

## [Version 3.0.1] - 2020-12-10 (par [1000i100])
### Ajouté
- test unitaire exectué dans la CI
- couverture de test
- suivi de la maintenabilité / complexité
- suivi de la duplication

## [Version 3.0.0] - 2020-12-10 (par [1000i100])

### Ajouté
- module g1lib.js séparé de Gsper



## [Version 2.1.0] - 2018-06-27 (par [1000i100])

### Ajouté
- syntaxe =référence> pour faire des références syncronisé et éviter de générer des variantes non souhaitées.

## [Version 2.0.0] - 2018-05-10 (par [1000i100])

### Ajouté

##### Générateur de variantes de mot de passe
- Déclinaisons avec Majuscules
- Désaccentuation
- Déclinaison avancées façon expression régulière
##### Documentation
- Rédaction d'une documentation des générateur de variante de mot de passe
##### Améliorations technique
- Ajout de test unitaire (meilleur fiabilité).
- Différentiation de la lib pour la partie crypto et de celle de génération de variantes (meilleur maintenabilité et évolutivité).

## [Version 1.0.1 (Proof of Concept)] - 2018-04-18 (par [1000i100])
### Ajouté

##### Algorithme
- intégration des librairies de crypto nécessaires
- calcul de la clef publique correspondant à chaque combinaison de secrets saisie, et comparaison à la clef publique de référence.

[Non-publié/Non-Stabilisé]: https://git.duniter.org/libs/g1lib.js/-/compare/v3.1.0...master

[Version 3.1.0]: https://git.duniter.org/libs/g1lib.js/-/compare/v3.0.3...v3.1.0
[Version 3.0.2]: https://git.duniter.org/libs/g1lib.js/-/compare/v3.0.1...v3.0.2
[Version 3.0.1]: https://git.duniter.org/libs/g1lib.js/-/compare/v3.0.0...v3.0.1
[Version 3.0.0]: https://git.duniter.org/libs/g1lib.js/-/compare/v2.1.0...v3.0.0
[Version 2.1.0]: https://git.duniter.org/tools/gsper/-/compare/v2.0.0...v2.1.0
[Version 2.0.0]: https://git.duniter.org/tools/gsper/-/compare/v1.0.1...v2.0.0
[Version 1.0.1 (Proof of Concept)]: https://git.duniter.org/tools/gsper/-/tree/v1.0.1

[1000i100]: https://framagit.org/1000i100 "@1000i100"
[Hugo]: https://trentesaux.fr/
