const fs = require('fs');
try {
	fs.writeFileSync('generated/vendors/nacl.mjs',
		(fs.readFileSync('node_modules/tweetnacl/nacl-fast.js', 'utf8'))
			.replace('(function(nacl) {', 'var nacl = {};')
			.replace('})(typeof module !== \'undefined\' && module.exports ? module.exports : (self.nacl = self.nacl || {}));', 'export default nacl;')
		, 'utf8');
} catch (error) {
	console.error(error);
}

try {
	fs.writeFileSync('generated/vendors/scrypt.mjs',
		(fs.readFileSync('node_modules/scrypt-async-modern/dist/index.js', 'utf8'))
			.replace('exports.default = scrypt;', 'export default scrypt;')
			.replace('Object.defineProperty(exports, "__esModule", { value: true });', '')
		, 'utf8');
} catch (error) {
	console.error(error);
}

