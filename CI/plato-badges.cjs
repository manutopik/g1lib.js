const fs = require('fs');
const {badgen} = require('badgen');

function badgeColor(percent) { // #9f9f9f lightgrey / inactive
	let color = 'e05d44'; // 'red';
	if (percent > 25) color = 'fe7d37'; // 'orange';
	if (percent > 50) color = 'dfb317'; // 'yellow';
	if (percent > 70) color = 'a4a61d'; // 'yellowgreen';
	if (percent > 80) color = '97ca00'; // 'green';
	if (percent > 90) color = '4c1'; // 'brightgreen';
	return color;
}

const report = require('../generated/maintainability/report.json');
const config = require('../package.json');
const maintainabilityThreshold = config.maintainabilityThreshold;

const avgMaintainabilityScore = Math.round(Number.parseFloat(report.summary.average.maintainability));
const avgMaintainabilityColor = badgeColor(avgMaintainabilityScore);
const worstMaintainabilityScore = Math.floor(report.reports.reduce((acc, cur) => Math.min(acc, Number.parseFloat(cur.complexity.maintainability)), 100));
const worstMaintainabilityColor = badgeColor(worstMaintainabilityScore);

const sloc = report.summary.total.sloc;

try {
	fs.writeFileSync('generated/avg-maintainability.svg', badgen({
		label: 'maintainability',
		status: `${avgMaintainabilityScore}`,
		color: avgMaintainabilityColor
	}), 'utf8');
} catch (error) {
	console.error(error);
}

try {
	fs.writeFileSync('generated/worst-maintainability.svg', badgen({
		label: 'maintainability',
		status: `${worstMaintainabilityScore}`,
		color: worstMaintainabilityColor
	}), 'utf8');
} catch (error) {
	console.error(error);
}

try {
	fs.writeFileSync('generated/total-sloc.svg', badgen({
		label: 'code lines',
		status: `${sloc}`,
		color: '007ec6' // 'blue'
	}), 'utf8');
} catch (error) {
	console.error(error);
}

if (avgMaintainabilityScore >= maintainabilityThreshold.global) {
	console.log(`Global project maintainability : ${avgMaintainabilityScore} ( >= ${maintainabilityThreshold.global} )`);
} else throw new Error(`Global project maintainability too low : ${avgMaintainabilityScore} < ${maintainabilityThreshold.global}`);
if (worstMaintainabilityScore >= maintainabilityThreshold.file) {
	console.log(`Worst project file maintainability : ${worstMaintainabilityScore} ( >= ${maintainabilityThreshold.file} )`);
} else throw new Error(`Worst project file maintainability too low : ${worstMaintainabilityScore} < ${maintainabilityThreshold.file}`);
