const fs = require('fs');
const toRollup = [];
rollupToDoList('generated/tmpBrowser/', 'generated/npm/browser/');
rollupToDoList('generated/tmpNodejs/', 'generated/npm/nodejs/');

function rollupToDoList(org, dest) {
	fs.readdirSync(org).forEach(
		fileName => (fileName.includes('.test') || !fileName.includes('.mjs')) ? 0 : toRollup.push(
			{
				input: `${org}${fileName}`,
				output: {
					file: `${dest}${fileName}`,
					format: 'esm'
				}
			}
		));
}

export default toRollup;
