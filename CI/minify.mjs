import {readdirSync, readFileSync, writeFileSync} from 'node:fs';
import {minify} from 'terser';

const minifyCache = {};
minFold('generated/npm/browser/');
minFold('generated/npm/nodejs/');

async function minFold(folder) {
	readdirSync(folder).forEach(async fileName => {
		if (!fileName.includes('.mjs')) return;
		const orgContent = readFileSync(folder + fileName, 'utf8');
		const minified = await minify(orgContent, {
			toplevel: true,
			ecma: 2020,
			nameCache: minifyCache,
			compress: {
				passes: 2
			},
			format: {
				comments: false
			}
		});
		writeFileSync(folder + fileName, minified.code);
	});
}
